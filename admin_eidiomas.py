from entidad_idiomas import idioma
import mysql.connector
from BD import BBDD

class Admin_idioma:
    def __init__(self):
        nuevaConexion = BBDD(True)
        self.__nuevaConexion = nuevaConexion
        self.__listaIdiomas = []
        self.__id = 0

    def Consulta_Tabla_idiomas_ALL(self):
        cursor = self.__nuevaConexion.GetConnectar().cursor()
        query = 'SELECT * FROM idiomas'
        cursor.execute(query)
        idiomas = cursor.fetchall()

        for idioma in idiomas:
            self.__listaIdiomas.append(idioma)
        
        cursor.close()
        self.__nuevaConexion.GetDesconectar()
        return self.__listaIdiomas
    
    def Consulta_Tabla_idiomas_BYID(self,pId):
        self.__id = pId
        cursor = self.__nuevaConexion.GetConnectar().cursor()
        query = "SELECT id, idioma FROM idiomas WHERE id = %i" % (self.__id)
        cursor.execute(query)
        self.__idiomaById = cursor.fetchall()
        
        cursor.close()
        self.__nuevaConexion.GetDesconectar()
        return self.__idiomaById

    def Consulta_Tabla_idiomas_BYIDIOMA(self,pIdioma):
        self.__idioma = pIdioma
        cursor = self.__nuevaConexion.GetConnectar().cursor()
        query = "SELECT id,idioma FROM idiomas WHERE idioma LIKE '%s'" % (self.__idioma)
        cursor.execute(query)
        self.__idiomaByIdioma = cursor.fetchall()
        
        cursor.close()
        self.__nuevaConexion.GetDesconectar()
        return self.__idiomaByIdioma

    def Actualizar_Tabla_idiomas_BYID(self,pId,pIdioma):
        self.__id = pId
        self.__idioma = pIdioma
        cursor = self.__nuevaConexion.GetConnectar().cursor()
        query = "UPDATE idiomas SET idioma = '%s' WHERE id = %i" % (self.__idioma, self.__id)
        cursor.execute(query)
        self.__nuevaConexion.GetConnectar.commit()
        cursor.close()

        cursorModificacion = self.__nuevaConexion.GetConnectar().cursor()
        query = "SELECT id, idioma FROM idiomas WHERE id = %i" % (self.__id)
        cursorModificacion.execute(query)
        self.__idiomaById = cursorModificacion.fetchall()
        
        cursorModificacion.close()
        self.__nuevaConexion.GetDesconectar()
        return self.__idiomaById

    def Insertar_Tabla_Idiomas_BYIDIOMA(self,pIdioma):
        self.__idioma = pIdioma
        cursor = self.__nuevaConexion.GetConnectar().cursor()
        query = "INSERT INTO idiomas (idioma) VALUE = '%s'" % (self.__idioma)
        cursor.execute(query)
        self.__nuevaConexion.commit()
        cursor.close()
        print("Insercion cambio")

        cursorModificacion = self.__nuevaConexion.GetConnectar().cursor()
        query = "SELECT * FROM idiomas"
        cursorModificacion.execute(query)
        self.__idiomas = cursorModificacion.fetchall()

        for idioma in self.__idiomas:
            self.__listaIdiomas.append(idioma)
        
        cursorModificacion.close()
        self.__nuevaConexion.GetDesconectar()
        return self.__listaIdiomas
