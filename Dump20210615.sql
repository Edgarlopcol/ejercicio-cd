CREATE DATABASE  IF NOT EXISTS `tienda_discos` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `tienda_discos`;
-- MySQL dump 10.13  Distrib 8.0.25, for Win64 (x86_64)
--
-- Host: localhost    Database: tienda_discos
-- ------------------------------------------------------
-- Server version	8.0.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cantantes`
--

DROP TABLE IF EXISTS `cantantes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cantantes` (
  `id` int NOT NULL AUTO_INCREMENT,
  `id_discografia` int NOT NULL,
  `id_nacionalidades` int NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `apellido` varchar(45) NOT NULL,
  `apodo` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_CANTANTES_DISCOGRAFICA_idx` (`id_discografia`),
  KEY `FK_CANTANTES_NACIONALIDAD_idx` (`id_nacionalidades`),
  CONSTRAINT `FK_CANTANTES_DISCOGRAFICA` FOREIGN KEY (`id_discografia`) REFERENCES `discograficas` (`id`),
  CONSTRAINT `FK_CANTANTES_NACIONALIDAD` FOREIGN KEY (`id_nacionalidades`) REFERENCES `nacionalidades` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cantantes`
--

LOCK TABLES `cantantes` WRITE;
/*!40000 ALTER TABLE `cantantes` DISABLE KEYS */;
INSERT INTO `cantantes` VALUES (1,3,3,'Kid','Pedro','Pedrito El Malito'),(2,3,2,'David','Guetta','El Guetta'),(3,2,4,'Pablo','Escobar','lo que sea'),(4,6,1,'Andrea','Bocelli','CuKiddfsf');
/*!40000 ALTER TABLE `cantantes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cd_cantantes`
--

DROP TABLE IF EXISTS `cd_cantantes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cd_cantantes` (
  `id_cantante` int NOT NULL,
  `id_cd` int NOT NULL,
  PRIMARY KEY (`id_cantante`,`id_cd`),
  KEY `FK_CDCANTANTES_CDS_idx` (`id_cd`),
  CONSTRAINT `FK_CDCANTANTES_CANTANTES` FOREIGN KEY (`id_cantante`) REFERENCES `cantantes` (`id`),
  CONSTRAINT `FK_CDCANTANTES_CDS` FOREIGN KEY (`id_cd`) REFERENCES `cds` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cd_cantantes`
--

LOCK TABLES `cd_cantantes` WRITE;
/*!40000 ALTER TABLE `cd_cantantes` DISABLE KEYS */;
INSERT INTO `cd_cantantes` VALUES (1,37),(4,38),(2,39),(3,40),(4,41),(3,42);
/*!40000 ALTER TABLE `cd_cantantes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cd_genero`
--

DROP TABLE IF EXISTS `cd_genero`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cd_genero` (
  `id_cd` int NOT NULL,
  `id_genero` int NOT NULL,
  PRIMARY KEY (`id_cd`,`id_genero`),
  KEY `FK_CDGENERO_GENERO_idx` (`id_genero`),
  CONSTRAINT `FK_CDGENERO_CD` FOREIGN KEY (`id_cd`) REFERENCES `cds` (`id`),
  CONSTRAINT `FK_CDGENERO_GENERO` FOREIGN KEY (`id_genero`) REFERENCES `generos` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cd_genero`
--

LOCK TABLES `cd_genero` WRITE;
/*!40000 ALTER TABLE `cd_genero` DISABLE KEYS */;
INSERT INTO `cd_genero` VALUES (37,1),(38,2),(41,3),(39,4),(40,4),(42,6);
/*!40000 ALTER TABLE `cd_genero` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cds`
--

DROP TABLE IF EXISTS `cds`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cds` (
  `id` int NOT NULL AUTO_INCREMENT,
  `id_compra` int NOT NULL,
  `id_idioma` int NOT NULL,
  `nombre` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_CDCOMPRA_idx` (`id_compra`),
  KEY `FK_CDIDIOMA_idx` (`id_idioma`),
  CONSTRAINT `FK_CDCOMPRA` FOREIGN KEY (`id_compra`) REFERENCES `compras` (`id`),
  CONSTRAINT `FK_CDIDIOMA` FOREIGN KEY (`id_idioma`) REFERENCES `idiomas` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cds`
--

LOCK TABLES `cds` WRITE;
/*!40000 ALTER TABLE `cds` DISABLE KEYS */;
INSERT INTO `cds` VALUES (37,1,3,'Thriller'),(38,2,3,'Black in Black'),(39,3,2,'El Guarda Espaldas'),(40,2,3,'Greatest Hits'),(41,4,1,'Rumors'),(42,2,2,'Sabado noche');
/*!40000 ALTER TABLE `cds` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `compras`
--

DROP TABLE IF EXISTS `compras`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `compras` (
  `id` int NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) NOT NULL,
  `apellido` varchar(45) NOT NULL,
  `fechacompra` date NOT NULL,
  PRIMARY KEY (`id`,`nombre`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `compras`
--

LOCK TABLES `compras` WRITE;
/*!40000 ALTER TABLE `compras` DISABLE KEYS */;
INSERT INTO `compras` VALUES (1,'Juan','Palomo','2011-10-08'),(2,'Pedro','Lozano','2010-11-03'),(3,'Edgar','Lopez','2010-08-02'),(4,'Daniel','Pascual','2009-09-12'),(5,'Albero','Medina','2008-03-13'),(6,'Frujencio','Lozano','2003-03-23');
/*!40000 ALTER TABLE `compras` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `discograficas`
--

DROP TABLE IF EXISTS `discograficas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `discograficas` (
  `id` int NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `discograficas`
--

LOCK TABLES `discograficas` WRITE;
/*!40000 ALTER TABLE `discograficas` DISABLE KEYS */;
INSERT INTO `discograficas` VALUES (1,'RCA'),(2,'Columbia'),(3,'Sony'),(4,'EMI'),(5,'WEA'),(6,'Warner');
/*!40000 ALTER TABLE `discograficas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `generos`
--

DROP TABLE IF EXISTS `generos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `generos` (
  `id` int NOT NULL AUTO_INCREMENT,
  `genero` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `generos`
--

LOCK TABLES `generos` WRITE;
/*!40000 ALTER TABLE `generos` DISABLE KEYS */;
INSERT INTO `generos` VALUES (1,'Blues'),(2,'Jazz'),(3,'Soul'),(4,'Pop'),(5,'Electro'),(6,'Rock');
/*!40000 ALTER TABLE `generos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `idiomas`
--

DROP TABLE IF EXISTS `idiomas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `idiomas` (
  `id` int NOT NULL AUTO_INCREMENT,
  `idioma` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `idiomas`
--

LOCK TABLES `idiomas` WRITE;
/*!40000 ALTER TABLE `idiomas` DISABLE KEYS */;
INSERT INTO `idiomas` VALUES (1,'Francés'),(2,'Español'),(3,'Inglés'),(4,'Italiano'),(5,'Brasileño'),(6,'Aleman');
/*!40000 ALTER TABLE `idiomas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nacionalidades`
--

DROP TABLE IF EXISTS `nacionalidades`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `nacionalidades` (
  `id` int NOT NULL AUTO_INCREMENT,
  `nacionalidad` varchar(45) NOT NULL,
  PRIMARY KEY (`id`,`nacionalidad`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nacionalidades`
--

LOCK TABLES `nacionalidades` WRITE;
/*!40000 ALTER TABLE `nacionalidades` DISABLE KEYS */;
INSERT INTO `nacionalidades` VALUES (1,'Inglés'),(2,'Francés'),(3,'Italiano'),(4,'Español'),(5,'Portugués');
/*!40000 ALTER TABLE `nacionalidades` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-06-15 13:06:12
